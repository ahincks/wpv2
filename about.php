<!DOCTYPE html>
<html lang="en">

<head>
  <title>WPV2</title>
  <link rel="stylesheet" type="text/css"
        href="lib/bootstrap/css/bootstrap.min.css">
</head>

<script type="text/javascript" src="lib/jquery.min.js"></script>
<script type="text/javascript"
        src="lib/bootstrap/js/bootstrap.min.js"></script>

<body>

<div class="container">

<div class="page-header">
  <h1>
    <tt>WPV2</tt>
    <small>Real-time display of dirfile data in your web browser.</small>
  </h1>
</div>

<h2>Example <small>Click the screen-shot for a live version.</small></h2>

<div style="width: 85%; padding-top: 15px;" class="center-block">
  <a href="example.php" data-toggle="tooltip" title="Hooray!">
    <img style="width: 100%;" src="screenshot.png">
  </a>
</div>

<h2>Information and Download</h2>

<p><tt>WPV2</tt> is a PHP and JavaScript package for viewing dirfile data
in your web browser. The dirfile format, designed for scientific data 
storage and used by several cosmology experiments, is described on the
<a href="http://getdata.sourceforge.net/"><tt>getdata</tt> homepage</a>, where
the official library for reading and writing dirfiles is 
provided. <tt>WPV2</tt> makes use of the PHP bindings provided by
<tt>getdata</tt>. It uses <a href="http://getbootstrap.com">bootstrap CSS 
styles</a>. <tt>WPV2</tt> is written and maintained by
<a href="http://adh-sj.info">Adam Hincks, S.J.</a> and is released under the 
<a href="http://www.gnu.org/licenses/gpl-3.0.en.html">GPL licence.</a>

<p>The most current code can be downloaded from
<a href="https://gitlab.com/ahincks/wpv2">Gitlab</a>, and you can
also download a release here (which is recommended for most use):</p>
<ul>
  <li><a href="releases/wpv2-1.0.0.zip">wpv2-1.0.0.zip</a>
  (1 March 2016)</li>
</ul>

<p><tt>WPV2</tt> requires the following PHP/JavaScript libraries (which come
bundled in the <tt>lib/</tt> path of the project):
<a href="https://jquery.com/">Jquery</a>,
<a href="http://getbootstrap.com/">Bootstrap</a>,
<a href="https://github.com/seiyria/bootstrap-slider">Bootstrap slider</a>,
<a href="http://colpick.com">Colpick</a>,
<a href="https://github.com/jacwright/date.format">Date.format</a>,
<a href="http://www.flotcharts.org/">Flot</a>,
<a href="https://github.com/markrcote/flot-axislabels">Flot-axislabels</a>,
<a href="https://github.com/janl/mustache.js/">Mustache</a>,
<a href="http://select2.github.io/select2/">Select2</a>,
<a href="https://fk.github.io/select2-bootstrap-css/">Select2 Boostrap 3 CSS</a>
and <a href="https://github.com/alexei/sprintf.js">Sprintf</a>.

<h2>Q&amp;A</h2>

<h3>Why do the CGI scripts use PHP and not, say, python?</h3>
<p>PHP running on Apache can create persistent resource handlers. Consequently,
multiple calls to PHP scripts from <tt>WPV2</tt> do not require reopening the
dirfile and reparsing the format file each time they are called.</p>

<h3>What does <tt>WPV2</tt> stand for?</h3>
<p>Fourteen years ago I wrote a stand-alone program (using 
<a href="http://www.qt.io/">Qt</a>) for viewing live dirfile data. It was called
<tt>palantir</tt>, in line with the 
<a href="http://en.wikipedia.org/wiki/The_Lord_of_the_Rings">LOTR</a> naming
scheme being used for the <a href="http://blastexperiment.info/">BLAST 
experiment</a> of the day. About eight years ago I made a web-based dirfile
viewer for <a href="http://princeton.edu/act">ACT</a>, called the
&lsquo;Web Palantir Viewer&rsquo; (<tt>WPV</tt>). The
original <tt>WPV</tt> used a custom-written <tt>PHP</tt> dirfile reader and
updates required page refreshes. <tt>WPV2</tt> is the successor to <tt>WPV</tt>
using the official <a href="http://getdata.sourceforge.net/">getdata</a>
library and AJAX calls instead of page refreshes.</p>

<h3>Why is this part titled &lsquo;Q&amp;A&rsquo; and not &lsquo;FAQ&rsquo;?</h3>
<p>Nobody has actually asked me any questions about <tt>WPV2</tt>, so there are
no frequent questions. Hence, I anticipate possible FAQs with a Q&amp;A.</p>

</div>
</body>
</html>
