<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css"
        href="lib/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css"
        href="lib/bootstrap-slider/css/bootstrap-slider.css">
  <link rel="stylesheet" type="text/css" href="lib/colpick/css/colpick.css">
  <link rel="stylesheet" type="text/css"
        href="lib/select2/dist/css/select2.min.css">
  <link rel="stylesheet" type="text/css" href="wpv2.css">
</head>

<script type="text/javascript" src="lib/jquery.min.js"></script>
<script type="text/javascript"
        src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="lib/bootstrap-slider/js/bootstrap-slider.js"></script>
<script type="text/javascript" src="lib/colpick/js/colpick.js"></script>
<script type="text/javascript"
        src="lib/select2/dist/js/select2.min.js"></script>
<script type="text/javascript" src="lib/mustache.js/mustache.min.js"></script>
<script type="text/javascript" src="lib/date.format.js"></script>
<script type="text/javascript"
        src="lib/sprintf.js/dist/sprintf.min.js"></script>
<script type="text/javascript" src="lib/flot/jquery.flot.js"></script>
<script type="text/javascript" src="lib/flot/jquery.flot.time.js"></script>
<script type="text/javascript" src="lib/flot/jquery.flot.resize.js"></script>
<script type="text/javascript" src="lib/jquery.flot.axislabels.js"></script>
<script type="text/javascript" src="settings.js"></script>

<?php
include("ajax/settings.php");
$template_name = array("styles_setup_numeric_head",
                       "styles_setup_datetime_head",
                       "styles_setup_assoc_head",
                       "styles_setup_minmax",
                       "styles_setup_delta_t",
                       "styles_setup_assoc",
                       "styles_setup_style",
                       "styles_setup_submit",
                       "grid_setup_box",
                       "edit_cell_head",
                       "edit_cell_graph",
                       "edit_cell_text_field",
                       "edit_cell_plot_field",
                       "edit_cell_spacer",
                       "edit_cell_new",
                       "alert_dismiss",
                       "move_buttons");
?>

<script>
var wstyle;
var styles_hash;
var default_cell = {title: "New Cell", update: 1000, content: [], type: "text"};
var template = {
  <? foreach($template_name as $t) { ?>
    <?=$t?>: "<?=str_replace(array(PHP_EOL, "\""), 
                             array("", "\\\""),
                             file_get_contents("template/" . $t . ".mst"));?>",
  <? } ?>
};
var datasrc;
var curr_frame;
var move_btn_styles = {move_remover: "$(this).parent().parent()",
                       move_container: "$('#styles_setup_container')",
                       move_allow_none: false};
var move_btn_text = {move_remover: "$(this).parent()",
                     move_container: "$(this).parents('.cell').first()",
                     move_allow_none: true};
var move_btn_plot = {move_remover: "$(this).parent().parent()",
                     move_container: "$(this).parents('.cell').first()",
                     move_allow_none: true};
var default_numeric_new = $.extend({}, move_btn_styles,
                                   {min: false, max: false, 
                                    bg: "#fff", fg: "#000", b: false, i: false,
                                    preview_label: "Preview",
                                    preview_text: sprintf("%f", 12.345),
                                    preview_input: false});
var default_datetime_new = $.extend({}, move_btn_styles,
                                    {ref_date: false, delta_t: false,
                                     bg: "#fff", fg: "#000", b: false, i: false,
                                     preview_label: "Preview",
                                     preview_text: "aldskjf",
                                     preview_input: false});
var default_assoc_new = $.extend({}, move_btn_styles,
                                 {input: "", output: "",
                                  bg: "#fff", fg: "#000", b: false, i: false,
                                  preview_label: "Output",
                                  preview_text: "",
                                  preview_input: true});
var plot_font = {size: 12,
                 family: "sans-serif",
                 variant: "small-caps",
                 color: "#333"};
var plot_option = {series: {shadowSize: 0},
                   grid: {margin: 0,
                          borderColor: "#777",
                          borderWidth: 1},
                   legend: {},
                   xaxis: {font: plot_font,
                           ticks: 4},
                   yaxis: {font: plot_font}
                  };

$(document).ready(function() {
  var regex, fname;

  // Settings.
  $("#grid_split_horiz").data("max", 12);
  $("#grid_split_horiz").data("weight", null);

  // Initialisation.
  $("#grid_menu").collapse({toggle: false});
  $("#styles_menu").collapse({toggle: false});
  $("#datasrc_menu").collapse({toggle: false});
  $("#save_menu").collapse({toggle: false});
  $('[data-toggle="tooltip"]').tooltip();

  // Set the data source buttons.
  $("#read_last").change(function() {
    var e = $("#read_start, #read_end, #read_rate");
    if ($(this).hasClass("active"))
      $(e).prop("disabled", false);
    else
      $(e).prop("disabled", true);
  });

  $.ajax({
    data: {search: "", default_only: true},
    method: "post",
    url: "ajax/get_save.php",
    type: "POST",
    success: function(r) {
      $("#load_layout").append("<option value='" + r.results[0].id + "'>" + 
                               r.results[0].text + "</option>");
      // Set up the select menu for loading files.
      $("#load_layout").select2({
        placeholder: "Select a layout",
        ajax: {
          quietMillis: 250,
          url: "ajax/get_save.php",
          dataType: "json",
          type: "POST",
          data: function (term) {
            return {
              search: typeof term.term == "undefined" ? "" : term.term,
              default_only: false
            };
          },
          results: function (data) {
            return {results: data};
          }
        }
      });
    }
  });

  regex = new RegExp("[\\?&]load=([^&#]*)");
  if ((fname = regex.exec(location.search)) === null)
    load("<?=$save_default_name?>");
  else {
    fname = decodeURIComponent(fname[1].replace(/\+/g, "_")) + ".wpv2";
    load(fname);
  }
});

function json_smart(json) {
  return JSON.parse(JSON.stringify(json), 
                    function(k, v) {
                      if (v === "false")
                        return false;
                      else if (v === "true")
                        return true;
                      else
                        return v;
                    }
                   );
}

function load(file) {
  $.ajax({
    data: {file: file},
    method: "post",
    url: "ajax/load.php",
    type: "POST",
    success: function(r) {
      var df;

      if (typeof r.err != "undefined") {
        $(Mustache.render(template.alert_dismiss, {msg: r.err, spacer: true}))
         .prependTo($("#container_main"));
        return;
      }
      
      wstyle = json_smart(r.style);
      datasrc = json_smart(r.datasrc);
      if (datasrc.last)
        curr_frame = -2;
      else
        curr_frame = datasrc.start;
      make_styles_hash();
      draw_grid(json_smart(r.col));
      $("#wpv_title").html(r.title);
      document.title = "WPV2 – " + r.title;
  
      if (datasrc.last)
        $("#read_last").button("toggle");
      $("#read_start").val(datasrc.start);
      $("#read_end").val(datasrc.end);
      $("#read_rate").val(datasrc.rate);
      datasrc.interval = null;

      df = (datasrc.dirfile == -1) ? curfile_name : datasrc.dirfile;
      $("#dirfile").append("<option value='" + datasrc.dirfile + "'>" + df + 
                           "</option>");

      // Set up the menu for selecting dirfiles.
      $("#dirfile").select2({
        ajax: {
          quietMillis: 250,
          url: "ajax/get_dir.php",
          dataType: "json",
          type: "POST",
          data: function (term) {
            return {
              recent: curfile_name,
              search: typeof term.term == "undefined" ? "" : term.term
            };
          },
          results: function (data) {
            return {results: data};
          }
        }
      });

      set_datasrc();
    }
  });
}

function set_datasrc() {
  datasrc.dirfile = $("#dirfile").val();
  datasrc.last = $("#read_last").hasClass("active");
  datasrc.start = parseInt($("#read_start").val());
  datasrc.end = parseInt($("#read_end").val());
  datasrc.rate = parseInt($("#read_rate").val());
  curr_frame = datasrc.start;
  if (datasrc.interval)
    clearInterval(datasrc.interval);

  if (!datasrc.last)
    datasrc.interval = setInterval(update_datasrc, datasrc.rate);
  else
    curr_frame = -2;

  $("#read_save").prop("disabled", true);
}


function draw_grid_setup() {
  var gs = $("#grid_setup_container");
  $(gs).empty();
  $("#container_main").children(".row").each(function() {
    var row = $("<div class='row'></row>").appendTo(gs);
    $(this).children(".col").each(function() {
      var col = $("<div class='" +  $(this).attr("class") + "'></div>")
                .appendTo(row);
      $(this).children(".cell").each(function() {
          cell = $(template.grid_setup_box).appendTo(col);
          $(cell).data("cell", $(this).data("cell"));
      });
    });
  });

  $("#wpv_title_input").val($("#wpv_title").html());

  $("#grid_accept_btn").prop("disabled", true);
}

function draw_grid(grid) {
  var main = $("#container_main");

  $(main).empty();
  $(grid).each(function() {
    var row = $("<div class='row'></row>").appendTo(main);
    $(this).each(function() {
      var col = $("<div class='col col-md-" + this.width + "'></div>")
                .appendTo(row);
      $(this.cell).each(function() {
        insert_cell(col, this);
      });
    });
  });
}

function get_bandwidth() {
  var bw, w, w_disp, e, ret;

  bw = 0;
  $("#container_main").children(".row").each(function() {
    $(this).children(".col").each(function() {
      $(this).children(".cell").each(function() {
        if ("content" in $(this).data("cell"))
          bw += $(this).data("cell").content.length * 1000.0 / 
                parseFloat($(this).data("cell").update);
      });
    });
  });

  w = sprintf("%.0f%%", bw / max_bandwidth * 100.0);
  if (bw <= max_bandwidth) {
    w_disp = w;
    $("#bw_bar").removeClass("progress-bar-danger");
    $("#bw_bar").addClass("active");
    ret = true;
  }
  else {
    w_disp = "100%";
    $("#bw_bar").addClass("progress-bar-danger");
    $("#bw_bar").removeClass("active");
    if (!$("#container_main").find("#alert_bandwidth").length) {
      e = $(Mustache.render(template.alert_dismiss,
                            {msg: "You have exceeded the allowed bandwidth."}))
            .prependTo($("#container_main"))
      $(e).attr("id", "alert_bandwidth");
      $(e).css("margin-top", "20px");
      toggle_update();
    }
    ret = false;
  }
  $("#bw_bar").css("width", w_disp);
  $("#bw_bar").html(w);

  return ret;
}

function make_styles_hash() {
  styles_hash = {};
  $(wstyle).each(function(i) {
    styles_hash[this.name] = i;
  });
}

function update_datasrc() {
  if (++curr_frame == datasrc.end)
    curr_frame = datasrc.start;
}

function check_style_remove_btn() {
  msc = $("#styles_setup_container");
  if ($(msc).children(".styles-setup-row").length == 1)
    $(msc).find(".btn-remove-row").prop("disabled", true);
  else
    $(msc).find(".btn-remove-row").prop("disabled", false);
}

function make_styles_menu(idx) {
  var i;

  // Draw the styles menu.
  ms = $("#styles_menu_select");
  $(ms).empty();
  for (i = 0; i < wstyle.length; i++) {
    $(ms).append("<option value='" + i + "'" + (i == idx ? " selected" : "") +
                 ">" + wstyle[i]["name"] + "</option>");
  }
  ms.select2();
  set_styles_menu(wstyle[idx]);
}

function set_style_colpick(row, selector) {
  $(row).find(selector).colpick({
    colorScheme:"dark",
    layout:"rgbhex",
    color:this["fg"],
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).css("background-color", "#" + hex);
      $(el).parents(".colour-box-col").
            prevAll(".preview-col").
            first().
            find(".preview").
            css("color", "#" + hex);
      $(el).colpickHide();
    }
  });
}

function styles_setup_newrow(html) {
  var msc, row;

  msc = $("#styles_setup_container");
  row = $(html).insertBefore($(msc).children(".styles-setup-submit").first());
  set_style_colpick($(row), ".colour-box-bg, .colour-box-fg");
  set_move_btns(msc, false);
}

function styles_setup_assoc_newrow() {
  styles_setup_newrow(Mustache.render(template.styles_setup_assoc,
                                      default_assoc_new,
                                      {style: template.styles_setup_style,
                                       move: template.move_buttons}));
}

function styles_setup_delta_t_newrow() {
  var fmt, preview, def;

  fmt = $("#style_setup_format").val();
      
  preview = new Date();
  preview = preview.format(fmt);
  def = default_datetime_new;
  def.preview_text = preview;
  styles_setup_newrow(Mustache.render(template.styles_setup_delta_t,
                                      def,
                                      {style: template.styles_setup_style,
                                       move: template.move_buttons}));
}

function styles_setup_numeric_newrow() {
  var fmt;

  fmt = $("#style_setup_format").val();

  styles_setup_newrow(Mustache.render(template.styles_setup_minmax,
                                      default_numeric_new,
                                      {style: template.styles_setup_style,
                                       move: template.move_buttons}));
}

function set_styles_menu(data) {
  var msc;
  
  msc = $("#styles_setup_container");
  $(msc).empty();
  switch (data.type) {
    case "assoc":
      $(msc).append(Mustache.render(template.styles_setup_assoc_head,
                                    {name: data["name"]}));
      $(msc).append(Mustache.render(template.styles_setup_submit,
                                    {type: "styles_setup_assoc"}));
      $(data.option).each(function(i) {
        var set = $.extend({}, move_btn_styles,
                           {input: this.input, output: this.output,
                            bg: this.bg, fg: this.fg, b: this.b, i: this.i,
                            preview_label: "Output",
                            preview_text: this.output,
                            preview_input: true});
        styles_setup_newrow(Mustache.render(template.styles_setup_assoc, set,
                                            {style: template.styles_setup_style,
                                             move: template.move_buttons}));
      });
      break;
    case "datetime":
      $(msc).append(Mustache.render(template.styles_setup_datetime_head,
                                    {name: data.name,
                                     format: data.format,
                                     suffix: data.suffix}));
      $(msc).append(Mustache.render(template.styles_setup_submit,
                                    {type: "styles_setup_delta_t"}));
      var preview = new Date();
      preview = preview.format(data.format);
      $(data.style).each(function(i) {
        var set = $.extend({}, move_btn_styles,
                           {ref_data: this.ref_data, delta_t: this.delta_t,
                            bg: this.bg, fg: this.fg, b: this.b, i: this.i,
                            preview_label: "Preview",
                            preview_text: preview,
                            preview_input: false});
        styles_setup_newrow(Mustache.render(template.styles_setup_delta_t, set,
                                            {style: template.styles_setup_style,
                                             move: template.move_buttons}));
      });
      break;
    case "numeric":
      $(msc).append(Mustache.render(template.styles_setup_numeric_head,
                                    {name: data["name"], 
                                     format: data["format"],
                                     units: data["units"]}));
      $(msc).append(Mustache.render(template.styles_setup_submit,
                                    {type: "styles_setup_numeric"}));
      $(data.style).each(function(i) {
        var set = $.extend({}, move_btn_styles,
                           {min: this.min, max: this.max,
                            bg: this.bg, fg: this.fg, b: this.b, i: this.i,
                            preview_label: "Preview",
                            preview_text: sprintf(data.format, 12.345),
                            preview_input: false});
        styles_setup_newrow(Mustache.render(template.styles_setup_minmax, set,
                                            {style: template.styles_setup_style,
                                             move: template.move_buttons}));
      });
      break;
  }
}

function styles_setup_assoc_submit() {
  var i, s, new_style, ssc, bad_row;
  
  ssc = $("#styles_setup_container");

  new_style = {};
  new_style["name"] = $("#style_setup_name").val();
  new_style["type"] = "assoc";
  if (!new_style["name"].length) {
    $(ssc).append(Mustache.render(template.alert_dismiss,
                   {msg: "You must enter a name for this style."}));
    return;
  }
  s = [];
  bad_row = false;
  $(ssc).children(".styles-setup-assoc").each(function(i) {
    s[i] = {};
    s[i]["input"] = $(this).find(".assoc-input").val();
    s[i]["output"] = $(this).find(".preview").val();
    s[i]["bg"] = $(this).find(".colour-box-bg").css("background-color");
    s[i]["fg"] = $(this).find(".colour-box-fg").css("background-color");
    s[i]["b"] = $(this).find(".btn-bold").hasClass("active");
    s[i]["i"] = $(this).find(".btn-italic").hasClass("active");
    if (!s[i]["input"].length || !s[i]["output"].length) {
      $(ssc).append(Mustache.render(template.alert_dismiss,
                     {msg: "You must enter a input and output values for " +
                           "row " + i + "."}));
      bad_row = true;;
    }
  });
  if (bad_row)
    return;
  new_style["option"] = s;
  if ($("#styles_menu_select").prop("new-style")) {
    wstyle.push(new_style);
    i = wstyle.length - 1; 
    $("#styles_menu_select").prop("new-style", false);
  }
  else {
    i = parseInt($("#styles_menu_select").val());
    wstyle[i] = new_style;
  }
  make_styles_hash();
  make_styles_menu(i);
}

function styles_setup_numeric_submit() {
  var i, s, new_style, ssc;

  ssc = $("#styles_setup_container");
  
  new_style = {};
  new_style["name"] = $("#style_setup_name").val();
  new_style["type"] = "numeric";
  new_style["format"] = $("#style_setup_format").val();
  new_style["units"] = $("#style_setup_units").val();
  if (!new_style["name"].length) {
    $(ssc).append(Mustache.render(template.alert_dismiss,
                   {msg: "You must enter a name for this style."}));
    return;
  }
  if (!new_style["format"].length) {
    $(ssc).append(Mustache.render(template.alert_dismiss,
                   {msg: "You must enter a format for this style."}));
    return;
  }
  s = [];
  $(ssc).children(".styles-setup-minmax").each(function(i) {
    s[i] = {};
    s[i]["min"] = $(this).find(".style-min").val();
    if (!s[i]["min"].length)
      s[i]["min"] = false;
    s[i]["max"] = $(this).find(".style-max").val();
    if (!s[i]["max"].length)
      s[i]["max"] = false;
    s[i]["bg"] = $(this).find(".colour-box-bg").css("background-color");
    s[i]["fg"] = $(this).find(".colour-box-fg").css("background-color");
    s[i]["b"] = $(this).find(".btn-bold").hasClass("active");
    s[i]["i"] = $(this).find(".btn-italic").hasClass("active");
  });
  new_style.style = s;
  if ($("#styles_menu_select").prop("new-style")) {
    wstyle.push(new_style);
    i = wstyle.length - 1; 
    $("#styles_menu_select").prop("new-style", false);
  }
  else {
    i = parseInt($("#styles_menu_select").val());
    wstyle[i] = new_style;
  }
  make_styles_hash();
  make_styles_menu(i);
}

function styles_setup_delta_t_submit() {
  var i, s, new_style, ssc;

  ssc = $("#styles_setup_container");
  
  new_style = {};
  new_style.name = $("#style_setup_name").val();
  new_style.type = "datetime";
  new_style.format = $("#style_setup_format").val();
  new_style.suffix = $("#style_setup_suffix").val();
  if (!new_style.name.length) {
    $(ssc).append(Mustache.render(template.alert_dismiss,
                   {msg: "You must enter a name for this style."}));
    return;
  }
  if (!new_style.format.length) {
    $(ssc).append(Mustache.render(template.alert_dismiss,
                   {msg: "You must enter a format for this style."}));
    return;
  }
  s = [];
  $(ssc).children(".styles-setup-delta-t").each(function(i) {
    s[i] = {};
    s[i].ref_date = $(this).find(".style-ref-date").val();
    if (!s[i].ref_date.length)
      s[i].ref_date = false;
    s[i].delta_t = $(this).find(".style-delta-t").val();
    if (!s[i].delta_t.length)
      s[i].delta_t = false;
    s[i].bg = $(this).find(".colour-box-bg").css("background-color");
    s[i].fg = $(this).find(".colour-box-fg").css("background-color");
    s[i].b = $(this).find(".btn-bold").hasClass("active");
    s[i].i = $(this).find(".btn-italic").hasClass("active");
  });
  new_style.style = s;
  if ($("#styles_menu_select").prop("new-style")) {
    wstyle.push(new_style);
    i = wstyle.length - 1; 
    $("#styles_menu_select").prop("new-style", false);
  }
  else {
    i = parseInt($("#styles_menu_select").val());
    wstyle[i] = new_style;
  }
  make_styles_hash();
  make_styles_menu(i);
}

function set_example_style(btn, tag, onval) {
  var eg;

  if (typeof onval == "string")
    onval = [onval];
  
  eg = $(btn).parents(".bi-col").
              prevAll(".preview-col").
              first().
              find(".preview");
  if (onval.indexOf($(eg).css(tag)) >= 0)
    $(eg).css(tag, "normal");
  else
    $(eg).css(tag, onval[0]);
}

function set_move_btns(div, allow_none) {
  // Check the delete button.
  if (!allow_none) {
    if ($(div).children(".move-row").length == 1)
      $(div).find(".btn-remove-row").prop("disabled", true);
    else
      $(div).find(".btn-remove-row").prop("disabled", false);
  }

  // Check the movement buttons.
  row = $(div).children(".move-row");
  $(row).find(".btn-move-row-up").prop("disabled", false);
  $(row).find(".btn-move-row-up").first().prop("disabled", true);
  $(row).find(".btn-move-row-down").prop("disabled", false);
  $(row).find(".btn-move-row-down").last().prop("disabled", true);
}

function move_row(btn, dir, allow_none) {
  me = $(btn).parents(".move-row");

  if (dir > 0)
    $(me).before($(me).next());
  else
    $(me).after($(me).prev());

  set_move_btns($(me).parent(), allow_none);
}

function add_spacer(adder) {
  $(Mustache.render(template.edit_cell_spacer, move_btn_text,
                    {move: template.move_buttons}))
                  .insertBefore(adder);
}

function add_field(adder, type) {
  var first, new_field, prev, new_field;

  first = $(adder).prevAll(".field-row").length ? false : true;
  new_field = null;
  switch (type) {
    case "text":
      new_field = $(Mustache.render(template.edit_cell_text_field,
                                    move_btn_text,
                                    {move: template.move_buttons}))
                  .insertBefore(adder);
      break;
    case "plot":
      new_field = $(Mustache.render(template.edit_cell_plot_field,
                                    $.extend({colour: "#aaa"}, move_btn_plot),
                                    {move: template.move_buttons}))
                  .insertBefore(adder);
      break;
  }
  set_move_btns($(adder).parents(".cell").first(), false);

  $.ajax({
    data: {dirfile: datasrc.dirfile},
    method: "post",
    url: "ajax/get_fields.php",
    type: "POST",
    success: function(r) {
      if (typeof r.err != "undefined") {
        $(Mustache.render(template.alert_dismiss, {msg: r.err}))
         .insertBefore(adder);
      }
      else {
        $(r).each(function() {
          $(new_field).find(".cell-field-select")
                      .append("<option value='" + this + "'>" + 
                              this + "</option>");
        });
        $(new_field).find(".cell-field-select").select2();
      }
    }
  });

  if (new_field) {
    switch (type) {
      case "text":
        $(wstyle).each(function(i) {
          $(new_field).children(".cell-style-select")
                      .append("<option value='" + i + "'>" +
                              this.name + "</option>");
        });
        break;
      case "plot":
        set_style_colpick(new_field, ".cell-colour");
        break;
    }
    $(new_field).children(".cell-style-select").select2();
  }
}

function toggle_update() {
  var s, e, b;

  e = $("#play_pause");
  b = get_bandwidth();

  s = $(e).children("span").first();
  if ($(s).hasClass("glyphicon-pause") || !b) {
    $(s).removeClass("glyphicon-pause");
    $(s).addClass("glyphicon-play");
    $("#bw_bar").removeClass("active");

    $("#container_main").find(".cell").each(function() {
      clearInterval($(this).data("cell").interval);
    });
  }
  else {
    $(s).removeClass("glyphicon-play");
    $(s).addClass("glyphicon-pause");
    $("#bw_bar").addClass("active");

    $("#container_main").find(".cell").each(function() {
      var data, gd_data;
      
      data = $(this).data("cell");
      gd_data = {"field": []};
      $(data.content).each(function(i) {
        gd_data.field[i] = this.field;
      });
      data.interval = setInterval(update_cell, data.update, gd_data, data);
      $(this).data("cell", data);
      update_cell(gd_data, data);
    });
  }
}

function edit_cell(cell) {
  var data;

  data = $(cell).data("cell");
  clearInterval(data.interval);
  $(cell).empty();

  $(cell).append(Mustache.render(template.edit_cell_head,
                                 {title: data.title,
                                  update: data.update}));
  $(cell).find(".cell-type").first().val(data.type);
  $(cell).find(".cell-type").select2()
         .on("change", function() {
           draw_cell_options($(this).parents(".cell").first());
         });
  draw_cell_options(cell);
}

function draw_cell_options(cell) {
  data = $(cell).data("cell");
  data.type = $(cell).find(".cell-type").first().val();
  if (typeof data.graph == "undefined")
    data.graph = {x_field: null, x_time: "", height: "300", n_frame: "60",
                  xlabel: "", ylabel: "", legend: "ne"};

  $(cell).children(".edit-cell-head").first().nextAll().remove();

  switch (data.type) {
    case "text":
      $(data.content).each(function(i) {
        if (this.field) 
          $(cell).append(Mustache.render(template.edit_cell_text_field,
                                         move_btn_text,
                                         {move: template.move_buttons}))
        else
          $(cell).append(Mustache.render(template.edit_cell_spacer,
                                         move_btn_text,
                                         {move: template.move_buttons}))
      });
      break;
    case "plot":
      $(cell).append(Mustache.render(template.edit_cell_graph,
                                     {time_fmt: data.graph.x_time,
                                      height: data.graph.height,
                                      n_frame: data.graph.n_frame,
                                      xlabel: data.graph.xlabel,
                                      ylabel: data.graph.ylabel}));
      $(".plot-time-fmt-addon").tooltip();
      $(".plot-legend").val(data.graph.legend);
      $(data.content).each(function() {
        $(cell).append(Mustache.render(template.edit_cell_plot_field,
                                       $.extend({colour: this.colour},
                                                move_btn_plot),
                                       {move: template.move_buttons}));
        set_style_colpick($(cell), ".cell-colour");
      });
      break;
  }
  set_move_btns(cell, false);
  $(cell).append(Mustache.render(template.edit_cell_new,
                                 {type: data.type,
                                  spacer: data.type == "text"}));

  $.ajax({
    data: {dirfile: datasrc.dirfile},
    url: "ajax/get_fields.php",
    type: "POST",
    success: function(r) {
      $(r).each(function() {
        $(".cell-field-select, .plot-x-axis")
          .append("<option value='" + this + "'>" + 
                                       this + "</option>");
      });
      $(cell).find(".cell-field-select").each(function(i) {
        $(this).val(data.content[i].field);
      });
      $(".plot-x-axis").val(data.graph.x_field);
      $(".cell-field-select, .plot-x-axis, .plot-legend").select2();
    }
  });

  if (data.type == "text") {
    $(wstyle).each(function(i) {
      $(".cell-style-select").append("<option value='" + i + "'>" +
                                     this.name + "</option>");
    });
    $(cell).find(".cell-style-select").each(function(i) {
      $(this).val(styles_hash[data.content[i].style]);
    });
    $(".cell-style-select").select2();
  }
}

function redefine_cell(cell) {
  var data, plot_opt;

  data = {};

  data.title = $(cell).find(".cell-title").first().val();
  data.update = $(cell).find(".cell-update").first().val();
  data.type = $(cell).find(".cell-type").first().val();

  if (data.update < min_update) {
    $(cell).append(Mustache.render(template.alert_dismiss,
                   {msg: "The update time must be at least " + min_update +
                         " ms."}));
    return;
  }

  data.content = [];
  switch (data.type) {
    case "text":
      $(cell).children(".field-row").each(function() {
        var f = $(this).children(".cell-field-select").first();
        if (f.length)
          data.content.push({field: f.val(),
                             style: wstyle[$(this)
                                             .children(".cell-style-select")
                                             .first().val()].name});
        else
          data.content.push({field: false});
      });
      break;
    case "plot":
      data.graph = {};
      data.graph.x_time = $(cell).find(".plot-time-fmt").first().val();
      data.graph.height = $(cell).find(".plot-height").first().val();
      data.graph.n_frame = $(cell).find(".plot-n-frame").first().val();
      data.graph.xlabel = $(cell).find(".plot-xlabel").first().val();
      data.graph.ylabel = $(cell).find(".plot-ylabel").first().val();
      data.graph.x_field = $(cell).find(".plot-x-axis").first().val();
      if (data.graph.n_frame > max_plot_frame) {
        $(cell).append(Mustache.render(template.alert_dismiss,
                       {msg: "The number of frames cannot exceed " + 
                             max_plot_frame + "."}));
        return;
      }
      if ((data.graph.legend = $(cell).find(".plot-legend").first().val()) ==
          "false")
        data.graph.legend = false;
      $(cell).children(".field-row").each(function() {
        data.content.push({field: $(this).find(".cell-field-select")
                                         .first().val(),
                           colour: $(this).find(".cell-colour")
                                          .first().css("background-color")});
      });
      break;
  }

  $(cell).data("cell", data);
  populate_cell(cell);
}

function populate_cell(cell) {
  var i, c, s, html, gd_data;

  data = $(cell).data("cell");

  clearInterval(data.interval);
  $(cell).empty();
  html = "<div class='cell-title'><h2>" + data.title + "</h2></div>" +
         "<div style='float:right;'>" +
         "<a href='#' " +
         "onclick='edit_cell($(this).parent().parent())'>" +
         "<span class='glyphicon glyphicon-edit'></span></a></div>" +
         "<div style='clear: both;'></div>" +
         "<div style='padding-top: 10px;'></div>";
  switch (data.type) {
    case "text":
      $(data.content).each(function() {
        if (this.field) {
          html += "<div class='cell-text'>" +
                  "<span class='cell-text-field'>" + this.field + "</span>" +
                  "<span class='cell-text-value' style=''>&mdash;" +
                  "</span><div style='clear: both;'></div></div>";
        }
        else
          html += "<div class='spacer'>&nbsp;</div>";
      });
      break;
    case "plot":
      html += "<div class='plot-y-label-container' style='height: " + 
              data.graph.height + "px;'>" +
              "<div class='plot-y-label'>" + data.graph.ylabel + 
              "</div></div>" +
              "<div class='plot' style='width: 100%; height: " + 
              data.graph.height + "px;'></div>" + 
              "<div class='plot-x-label'>" + data.graph.xlabel + "</div>";
      break;
  }
  $(cell).append(html);

  if (data.type == "plot") {
    var p = $(cell).children(".plot").first();
    var lc = $(cell).children(".plot-y-label-container").first();
    var l = $(lc).children(".plot-y-label").first();

    // Position the y-label.
    $(lc).width($(l).height() + 10);
    $(l).css({top: $(lc).height() / 2.0 + $(l).width() / 2});
    $(p).css({left: $(lc).width()});
    $(p).width($(cell).width() - $(lc).width());

    data.plot_field = [];
    $(data.content).each(function() {
      data.plot_field.push({label: this.field, data: []});
    });

    plot_opt = plot_option;
    if (data.graph.legend) {
      plot_opt.legend.show = true;
      plot_opt.legend.position = data.graph.legend;
    }
    else
      plot_opt.legend.show = false;
    if (data.graph.x_time) {
      plot_opt.xaxis.mode = "time";
      plot_opt.xaxis.timeformat = data.graph.x_time;
    }
    plot_opt.colors = [];
    $(data.content).each(function() {
      plot_opt.colors.push(this.colour);
    });

    data.plot = $.plot($(cell).children(".plot"), data.plot_field, plot_opt);
  }
  data.div = cell;
  gd_data = {"field": []};
  $(data.content).each(function() {
    gd_data.field.push(this.field);
  });
  if (data.type == "plot")
    gd_data.field.push(data.graph.x_field);

  if (get_bandwidth()) {
    data.interval = setInterval(update_cell, data.update, gd_data, data);
    $(data.div).data("cell", data);
    update_cell(gd_data, data);
  }
  else
    $(data.div).data("cell", data);
}


function insert_cell(col, data) {
  var cell;

  cell = $("<div class='cell'></div").appendTo(col);

  $(cell).data("cell", data);

  populate_cell(cell);
}

function style_string(s) {
  return "color: " + s.fg + ";" +
         "background-color: " + s.bg + ";" +
         "font-weight: " + (s.b ? "bold" : "normal") + ";" +
         "font-style: " + (s.i ? "italic" : "normal") + ";";
}

function update_cell(gd_data, data) {
  gd_data.dirfile = datasrc.dirfile;
  if (data.type == "text") {
    gd_data.n_frame = 1;
    gd_data.frame = curr_frame;
  }
  else {
    gd_data.n_frame = data.graph.n_frame - data.plot_field[0].data.length + 1;
    gd_data.frame = curr_frame - gd_data.n_frame + 1;
  }
  $.ajax({
    url: "ajax/get.php",
    type: "POST",
    data: gd_data,
    success: function(r) {
      var val, i;

      if (typeof r.err != "undefined") {
        if (!$("#container_main").children(".alert").length)
          $(Mustache.render(template.alert_dismiss, {msg: r.err, spacer: true}))
                    .prependTo($("#container_main"));
        return;
      }

      $("#frame_display").html((parseInt(r.frame) + 1) + " of " + r.nframes);

      switch (data.type) {
        case "text":
          $(data.div).children(".cell-text").each(function(i) {
            var s, j, my_style;

            my_style = null;
            val = null;
            if ((j = styles_hash[data.content[i].style]) == null)
              j = 0;
            s = wstyle[j];
        
            switch (s.type) {
              case "assoc":
                $(s.option).each(function() {
                  if (val)
                    return;
                  if (parseFloat(r.data[i][0]) == this.input) {
                    val = this.output;
                    my_style = style_string(this);
                  }
                });
                break;
              case "datetime":
                var d, u_d, now, delta_t;
                u_d = parseFloat(r.data[i][0]);
                d = new Date(u_d * 1000);
                val = d.format(s.format);
                if (s.suffix)
                  val += "&nbsp;" + s.suffix;
                $(s.style).each(function() {
                  if (my_style)
                    return;
                  if (this.ref_date)
                    now = new Date(this.ref_date * 1000.0);
                  else
                    now = new Date();
                  delta_t = now.getTime() / 1000.0 - u_d;
                  if (this == s.style[s.style.length - 1] || Math.abs(delta_t) >
                    parseFloat(this.delta_t))
                    my_style = style_string(this);
                });
                break;
              case "numeric": default:
                val = sprintf(s.format, parseFloat(r.data[i][0]));
                $(s.style).each(function() {
                  if (my_style)
                    return;
                  if (this == $(s.style).last() ||
                      (((this.max && parseFloat(val) < this.max) ||
                         !this.max) &&
                       ((this.min && parseFloat(val) > this.min) || !this.min)))
                    my_style = style_string(this);
                });
                val += s.units;
                break;
            }
            if (!val)
              val = "N/A";
            $(this).children(".cell-text-value").last().html(val);
            $(this).children(".cell-text-value").last().attr("style", my_style);
          });
          break;
        case "plot":
          var x, mult;

          x = r.data[r.data.length - 1];
          mult = data.graph.x_time ? 1000.0 : 1.0;
          
          $(data.content).each(function(i) {
            $(x).each(function(j) {
              data.plot_field[i].data.push([this * mult, r.data[i][j]]);
            });
            while (data.plot_field[i].data.length > data.graph.n_frame)
              data.plot_field[i].data.shift();
          });
          data.plot.setupGrid();
          data.plot.setData(data.plot_field);
          data.plot.draw();
          break;
      }
    }
  })
}

function grid_setup_cell_html(w) {
  return "<div class='col col-md-" + w.toString() + "'>" + 
         template.grid_setup_box + "</div>";
}

function grid_setup_col_width(e) {
  cls = $(e).attr("class").split(" ");
  for (i = 0; i < cls.length; i++) {
    if (cls[i].substring(0, 7) == "col-md-")
      return parseInt(cls[i].substring(7));
  }

  return null;
}

function analyse_grid_setup() {
  var adjacent_col, adjacent_subrow, done, check, single_subrow, single_width;
  var single_check, single, n_check;

  single = false;
  adjacent_col = false;
  adjacent_subrow = false;
  done = false;
  check = false;
  n_check = 0;
  single_subrow = 0;
  single_width = 0;
  single_check = null;

  $("#grid_setup_container").children(".row").each(function() {
    var first_col = true;
    $(this).children(".col").each(function() {
      var first_subrow = true;
      var col = this;
      var n_subrow = $(this).children(".grid-setup").length;
      $(this).children(".grid-setup").each(function() {
        if ($(this).children("input").first().prop("checked")) {
          if ((first_col && (adjacent_subrow || adjacent_col || n_check)) ||
              (first_subrow && n_subrow > 1 && n_check) ||
              (first_subrow && adjacent_subrow)) {
            done = true;
            return;
          }
          if (check) {
            if (n_subrow == 1)
              adjacent_col = true;
            else
              adjacent_subrow = true;
          }
          else if (adjacent_col || adjacent_subrow) {
            done = true;
            return;
          }
          n_check++;
          single_subrow = n_subrow;
          single_width = grid_setup_col_width(col);
          single_check = col;
          check = true;
          first_col = false;
          first_subrow = false;
        }
        else
          check = false;
      });
      if (done)
        return;
    });
    if (done) {
      adjacent_col = false;
      adjacent_subrow = false;
      n_check = 0;
      return;
    }
  });

  single = (n_check == 1 && single_width > 1) ? single_check : null;

  return {single: single, adj_col: adjacent_col, adj_srow: adjacent_subrow,
          single_srow: single_subrow, single_width: single_width};
}

function check_grid_setup_option() {
  var a;

  a = analyse_grid_setup();
  $("#grid_split_horiz").prop("disabled", (a.single && a.single_srow == 1 &&
                                          a.single_width > 1) ? false : true);
  $("#grid_split_vert").prop("disabled", a.single ? false : true);
  $("#grid_join_cells").prop("disabled", a.adj_col || a.adj_srow ? false: true);
}

function grid_split_horiz_html() {
  var e1, e2, w1, w2;

  e1 = $("#grid_split_horiz").data("e");
  w1 = parseInt($("#grid_split_horiz").data("weight").slider("getValue"));
  w2 = parseInt($("#grid_split_horiz").data("max") - w1);
  $(e1).attr("class", "col col-md-" + w1.toString());
  e2 = $(grid_setup_cell_html(w2)).insertAfter(e1);
  $(e1).children(".grid-setup").children("input")
                               .first().prop("checked", false);
  $(e2).children(".grid-setup").first().data("cell", default_cell);
  $("#grid_split_horiz").prop("disabled", true);
  $("#grid_split_vert").prop("disabled", true);

  $("#grid_accept_btn").prop("disabled", false);
}

function grid_split_horiz() {
  var a;
  a = analyse_grid_setup();
  $("#grid_split_horiz").data("max", a.single_width);
  $("#grid_setup_split_body").empty();
  html = "<input type='text' id='grid_setup_split_weight' " +
                "data-slider-id='grid_setup_split_weight_slider' " +
                "data-slider-min='1' data-slider-step='1' " +
                "data-slider-max='" + (a.single_width - 1) + "' " +
                "data-slider-value='" + ((a.single_width / 2) + 1) + "' " +
                "style='width: 100%;'/>";
  $("#grid_setup_split_body").append(html);
  $("#grid_split_horiz").data("weight", 
                              $("#grid_setup_split_weight").slider({
                                formatter: function(val) {
                                  return val + "/" + 
                                         ($("#grid_split_horiz").data("max") - 
                                         val);
                                }
                              }));
  $("#grid_split_horiz").data("e", a.single);
  $("#grid_setup_split_modal").modal({backdrop: false});
}

function grid_split_vert() {
  var a, e, html;

  a = analyse_grid_setup();
  html = "<div class='grid-setup'>" +
         "<input type='checkbox' onclick='check_grid_setup_option()'>" +
         "</div>";
  e = $(html).appendTo(a.single);
  $(e).data("cell", default_cell);
  $(a.single).children(".grid-setup").children("input").prop("checked", false);
  $("#grid_split_horiz").prop("disabled", true);
  $("#grid_split_vert").prop("disabled", true);

  $("#grid_accept_btn").prop("disabled", false);
}

function grid_join_cells() {
  var a, e_first, e_first_col, w;

  a = analyse_grid_setup();
  e_first = null;
  e_first_col = null;
  w = 0;

  $("#grid_setup_container").children(".row").each(function() {
    $(this).children(".col").each(function() {
      var col = this;
      $(this).children(".grid-setup").each(function() {
        if ($(this).children("input").first().prop("checked")) {
          if (a.adj_col)
            w += grid_setup_col_width(col);
          if (!e_first)
            e_first = this;
          else
            $(this).remove();
          $(this).children("input").first().prop("checked", false);
        }
      });
    });
  });
  if (a.adj_col)
    $(e_first).parent().attr("class", "col col-md-" + w.toString());
  $("#grid_join_cells").prop("disabled", true);

  $("#grid_accept_btn").prop("disabled", false);
}

function grid_add_row() {
  var e, html;

  html = "<div class='row'>" + grid_setup_cell_html(12) + "</div>";
  e = $(html).appendTo($("#grid_setup_container"));
  $(e).find(".grid-setup").first().data("cell", default_cell);
  $("#grid_remove_row").prop("disabled", false);

  $("#grid_accept_btn").prop("disabled", false);
}

function grid_remove_row() {
  c = $("#grid_setup_container");
  n = c.children(".row").length;
  c.children(".row")[n - 1].remove();
  if (n <= 2)
    $("#grid_remove_row").prop("disabled", true);

  $("#grid_accept_btn").prop("disabled", false);
}

function grid_accept() {
  var col;

  col = [];
  $("#grid_setup_container").children(".row").each(function() {
    var this_col = [];

    $(this).children(".col").each(function() {
      var cell = {};

      cell.width = grid_setup_col_width(this);
      cell.cell = [];
      $(this).children(".grid-setup").each(function() {
        cell.cell.push($(this).data("cell"));
      });

      this_col.push(cell);
    });

    col.push(this_col);
  });

  $("#grid_menu").collapse("hide");
  $("#wpv_title").html($("#wpv_title_input").val());
  document.title = "WPV2 – " + $("#wpv_title_input").val();
  draw_grid(col);

  return;
}

function cycle_btn_active(e) {
  if ($(e).hasClass("active"))
    $(e).removeClass("active");
  else
    $(e).addClass("active");
}

function save_layout(force) {
  var s;

  save = {};
  save.title = $("#wpv_title").html();
  save.datasrc = datasrc;
  save.style = wstyle;
  save.col = [];
  $("#container_main").children(".row").each(function() {
    var row = []; 
    $(this).children(".col").each(function() {
      var col = {};
      col.width = grid_setup_col_width(this);
      col.cell = [];
      $(this).children(".cell").each(function() {
        var cell = $.extend(true, {}, $(this).data("cell"));
        delete cell.div;
        delete cell.interval;
        delete cell.plot;
        delete cell.plot_field;
        col.cell.push(cell);
      });
      row.push(col);
    });
    save.col.push(row);
  });

  $.ajax({
    data: {json: save,
           force: force, 
           default: $("#file_default").hasClass("active")
          },
    dataType: "json",
    method: "post",
    url: "ajax/save.php",
    type: "POST",
    success: function(r) {
      if (typeof r.err != "undefined") {
        $(Mustache.render(template.alert_dismiss, {msg: r.err, spacer: true}))
         .prependTo($("#save_menu").children(".container").first());
      }
      else if (typeof r.exists != "undefined") {
        $("#save_confirm_title").html($("#wpv_title").html());
        $("#save_confirm_modal").modal({backdrop: false});
      }
      else {
        $(Mustache.render(template.alert_dismiss,
                          {msg: r.msg, spacer: true, type: "success"}))
          .prependTo($("#container_main")); 
        $("#save_menu").collapse("hide");
      }
    }
  });
}


</script>

<body>

<div class="navbar navbar-static-top navbar-inverse navbar-flush-bottom" 
      role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" 
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">WPV2 &ndash; 
        <span id="wpv_title">Untitled</span>
      </a>
    </div>
    <button type="button" class="btn btn-primary navbar-btn navbar-left"
            id="play_pause"  onclick="toggle_update();"
            data-toggle="tooltip" data-placement="bottom"
            title="Start/pause updates.">
      &nbsp;<span class="glyphicon glyphicon-pause"></span>&nbsp;
    </button>
    <div class="navbar-left navbar-text progress" style="width: 100px;"
         data-toggle="tooltip" data-placement="bottom" 
         title="Amount of allowed bandwidth being used.">
      <div class="progress-bar progress-bar-striped active" id="bw_bar"
           role="progressbar" aria-valuenow="" aria-valuemin="0"
           aria-valuemax="100" style="width: 0%;">
        0%
      </div>
    </div>
    <div class="navbar-left navbar-text">
      <strong id="frame_display"></strong>
    </div>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <p class="navbar-btn">
          <button type="button" class="btn btn-default" id="datasrc_menu_btn"
                  data-target="#datasrc_menu" data-toggle="collapse"
                  onclick="cycle_btn_active(this);">
            &nbsp;<span class="glyphicon glyphicon-hdd"></span>&nbsp;
          </button>
        </p>
      </li><li>
        <p class="navbar-btn">
          &nbsp;
          <button type="button" class="btn btn-default" id="grid_menu_btn"
                  data-target="#grid_menu" data-toggle="collapse"
                  onclick="cycle_btn_active(this);">
            &nbsp;<span class="glyphicon glyphicon-th"></span>&nbsp;
          </button>
        </p>
      </li><li>
        <p class="navbar-btn">
          &nbsp;
          <button type="button" class="btn btn-default" id="styles_menu_btn"
                  data-target="#styles_menu" data-toggle="collapse"
                  onclick="cycle_btn_active(this);">
            &nbsp;<span class="glyphicon glyphicon-font"></span>&nbsp;
          </button>
        </p>
      </li><li>
        <p class="navbar-btn">
          &nbsp;
          <button type="button" class="btn btn-default" id="save_menu_btn"
                  data-target="#save_menu" data-toggle="collapse"
                  onclick="cycle_btn_active(this);">
            &nbsp;<span class="glyphicon glyphicon-folder-open"></span>&nbsp;
          </button>
        </p>
      </li><li>&nbsp;&nbsp;</li>
    </ul>
  </div>
</div>

<div class="below-nav-collapse collapse" id="save_menu">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-4">
        <div class="form-group">
          <button type="button" class="btn btn-success btn-group-justified"
                  onclick="save_layout(false);">
            Save Current Layout
          </button>
        </div>
      </div><div class="col-md-2">
        <div class="btn-group" data-toggle="buttons">
          <label id="file_default" class="btn btn-primary btn-group-justified">
            <input type="checkbox" autocomplete="off">
            Make Default
          </label>
        </div>
      </div>
    </div><div class="row">
      <div class="col-md-offset-3 col-md-6">
        <div class="input-group select2-bootstrap-prepend">
          <span class="input-group-addon">
            Load a Layout
          </span>
          <select class="form-control" id="load_layout" style="width: 100%;"
                  onchange="load(this.value);">
          </select>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="below-nav-collapse collapse" id="datasrc_menu">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-4">
        <p>
          <select class="form-control" id="dirfile" style="width: 100%;"
                  onchange="$('#read_save').prop('disabled', false);">
          </select>
        </p>
      </div><div class="col-md-3">
        <div class="form-group">
          <div class="btn-group" data-toggle="buttons">
            <label id="read_last" class="btn btn-primary btn-group-justified">
              <input type="checkbox" autocomplete="off"
                     onchange="$('#read_save').prop('disabled', false);">
              Read last frame
            </label>
          </div>
        </div>
      </div>
    </div><div class="row">
      <div class="col-md-offset-2 col-md-2">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">Start frame</span>
            <input id="read_start" class="form-control" type="text"
                   onchange="$('#read_save').prop('disabled', false);"
                   placeholder="0">
          </div>
        </div>
      </div><div class="col-md-2">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">End frame</span>
            <input id="read_end" class="form-control" type="text"
                   onchange="$('#read_save').prop('disabled', false);"
                   placeholder="-1">
          </div>
        </div>
      </div><div class="col-md-3">
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">Step Rate</span>
            <input id="read_rate" class="form-control" type="text"
                   onchange="$('#read_save').prop('disabled', false);"
                   placeholder="1000">
            <span class="input-group-addon">ms</span>
          </div>
        </div>
      </div>
    </div><div class="row">
      <div class="col-md-offset-2 col-md-7">
        <div class="form-group">
          <button type="button" id="read_save"
                  class="btn btn-success btn-group-justified"
                  onclick="set_datasrc();">
            Save
          </button>
        </div>
      </div><div class="col-md-3">
      </div>
    </div>
  </div>
</div>
<div class="below-nav-collapse collapse" id="grid_menu">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <div class="input-group">
          <span class="input-group-addon">Title</span>
          <input type="text" id="wpv_title_input" class="form-control"
                 placeholder="Enter a short title" value=""
                 onchange="$('#grid_accept_btn').prop('disabled', false);"/>
        </div>
      </div>
    </div><div class="row margin-top">
      <div class="col-md-offset-3 col-md-2">
        <button type="button" class="btn btn-default btn-group-justified"
                id="grid_join_cells" onclick="grid_join_cells();" disabled>
          Join
        </button>
      </div><div class="col-md-2">
        <button type="button" class="btn btn-default btn-group-justified"
                id="grid_split_horiz" onclick="grid_split_horiz();" disabled>
          Horizontal Split 
        </button>
      </div><div class="col-md-2">
        <button type="button" class="btn btn-default btn-group-justified"
                id="grid_split_vert" onclick="grid_split_vert();" disabled>
          Vertical Split 
        </button>
      </div>
    </div><div class="row">
      <div class="col-md-offset-3 col-md-6" id="grid_setup_container">
      </div>
    </div><div class="row margin-top">
      <div class="col-md-offset-3 col-md-3">
        <button type="button" class="btn btn-default btn-group-justified"
                onclick="grid_add_row();">
          Add Row
        </button>
      </div><div class="col-md-3">
        <button type="button" class="btn btn-default btn-group-justified"
                id="grid_remove_row" onclick="grid_remove_row();">
          Remove Row
        </button>
      </div>
    </div><div class="row margin-top">
      <div class="col-md-6 col-md-offset-3">
        <button type="button" id="grid_accept_btn"
                class="btn btn-success btn-group-justified" disabled
                onclick="grid_accept(); $('#grid_menu').collapse();">
          Implement
        </button>
      </div>
    </div>
  </div>
</div>
<div class="below-nav-collapse collapse" id="styles_menu">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="input-group select2-bootstrap-append">
          <select class="form-control" id="styles_menu_select"
                  onchange="set_styles_menu(wstyle[$(this).val()]);">
          </select>
          <div class="input-group-btn">
            <button type="button" class="btn btn-primary"
                    onclick="$('#new_style_modal').modal({backdrop: false})">
              <span class="glyphicon glyphicon-plus"></span>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container" id="styles_setup_container">
  </div>
</div>
<script>
  $("#styles_menu").on("show.bs.collapse", function() {
    $("#grid_menu, #datasrc_menu, #save_menu").collapse("hide");
    $("#grid_menu_btn, #datasrc_menu_btn, #save_menu_btn")
      .removeClass("active");
  });
  $("#styles_menu").on("shown.bs.collapse", function() {
    make_styles_menu(0); 
  });
  $("#grid_menu").on("show.bs.collapse", function() {
    $("#styles_menu, #datasrc_menu, #save_menu").collapse("hide");
    $("#styles_menu_btn, #datasrc_menu_btn, #save_menu_btn")
      .removeClass("active");
  });
  $("#grid_menu").on("shown.bs.collapse", function() {
    draw_grid_setup(); 
  });
  $("#datasrc_menu").on("show.bs.collapse", function() {
    $("#styles_menu, #grid_menu, #save_menu").collapse("hide");
    $("#styles_menu_btn, #grid_menu_btn, #save_menu_btn").removeClass("active");
  });
  $("#datasrc_menu").on("shown.bs.collapse", function() {
  });
  $("#save_menu").on("show.bs.collapse", function() {
    $("#styles_menu, #grid_menu, #datasrc_menu").collapse("hide");
    $("#styles_menu_btn, #grid_menu_btn, #datasrc_menu_btn")
      .removeClass("active");
  });
  $("#datasrc_menu").on("shown.bs.collapse", function() {
  });
</script>

<div class="modal fade" id="save_confirm_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        A layout with the title 
        &lsquo;<span id="save_confirm_title"></span>&rsquo; has already been
        saved. Do you want to replace it?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"
                onclick="$('#save_menu').collapse('hide');">
          No
        </button>
        <button id="grid_setup_split_accept" type="button" 
                class="btn btn-primary" data-dismiss="modal"
                onclick="save_layout(true);">
          Yes
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="grid_setup_split_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Specify Split</h4>
      </div>
      <div class="modal-body" id="grid_setup_split_body">
        <div></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
          Cancel
        </button>
        <button id="grid_setup_split_accept" type="button" 
                class="btn btn-primary" data-dismiss="modal"
                onclick="grid_split_horiz_html();">
          Accept
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="new_style_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
                aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Choose a Style Type</h4>
      </div>
      <div class="modal-body text-center" id="grid_setup_split_body">
        <div class="btn-group" role="group" aria-label="Style Options">
          <button type="button" class="btn btn-default" data-dismiss="modal"
                  onclick="set_styles_menu({name: '', type: 'numeric',
                                            format: '%f', units: '',
                                            style: [default_numeric_new]});
                           $('#styles_menu_select').prop('new-style', true);">
            Numeric
          </button>
          <button type="button" class="btn btn-default" data-dismiss="modal"
                  onclick="set_styles_menu({name: '', type: 'datetime',
                                            format: 'j M Y, G:i:s',
                                            suffix: '',
                                            style: [default_datetime_new]});
                           $('#styles_menu_select').prop('new-style', true);">
            Datetime
          </button>
          <button type="button" class="btn btn-default" data-dismiss="modal"
                  onclick="set_styles_menu({name: '', type: 'assoc',
                                            option: [default_assoc_new]});
                           $('#styles_menu_select').prop('new-style', true);">
            Associative
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid" id="container_main">
</div>

</body>
</html>
