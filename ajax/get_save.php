<?php
include("all.php");

$ret["results"] = array();
$default = @readlink($save_dir . "/" . $save_default_name);

function make_text($f, $def) {
  if ($f == $def)
    $text = substr($f, 0, -5) . " (default)";
  else
    $text = substr($f, 0, -5);
  $text = str_replace(array("_"), array(" "), $text);

  return $text;
}

if (filter_var($_POST["default_only"], FILTER_VALIDATE_BOOLEAN) === true) {
  $ret["results"] = array(array("id" => basename($default),
                                "text" => make_text(basename($default), "")));
  reply_to_ajax();
}

if ($dh = opendir($save_dir)) {
  $df = array();
  while (($de = readdir($dh)) !== false) {
    if (!is_string($de) || substr($de, -5) != ".wpv2")
      continue;
    if (strlen($_POST["search"]) && strpos($de, $_POST["search"]) === false)
      continue;
    array_push($df, $de);
  }
  rsort($df);
  foreach ($df as $de) {
    array_push($ret["results"], array("id" => $de,
                                      "text" => make_text($de, $default)));
  }
}
else
  return_error("Could not open " . $save_dir . ".");

reply_to_ajax();
?>
