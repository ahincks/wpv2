<?php
include("all.php");

$path = $save_dir . "/" . 
        str_replace(array(" "), array("_"), $_POST["json"]["title"]) . ".wpv2";

if (filter_var($_POST["force"], FILTER_VALIDATE_BOOLEAN) === false && 
    file_exists($path)) {
  $ret["exists"] = true;
  reply_to_ajax();
}

if (@file_put_contents($path, json_encode($_POST["json"])) === false)
  return_error("Could not write file " . $path . ".");
if (filter_var($_POST["default"], FILTER_VALIDATE_BOOLEAN) === true) {
  $link = $save_dir . "/" . $save_default_name;
  @unlink($link);
  if (!@symlink($path, $link))
    return_error("Could not set default symlink.");
}

$ret["msg"] = "Layout successfully saved.";
reply_to_ajax();

?>
