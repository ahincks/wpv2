<?php
include("all.php");

$df = get_dirfile();

if (($frame = $_POST["frame"]) < 0) {
  if (($frame += gd_nframes($df)) < 0)
    $frame = 0;
}
$n_frame = $_POST["n_frame"];

$ret["data"] = array();
$ret["nframes"] = gd_nframes($df);
$ret["frame"] = $frame;

foreach ($_POST["field"] as $field) {
  $a = array();
  for ($i = 0; $i < $n_frame; $i++) {
    $x = gd_getdata($df, $field, $frame + $i, 0, 0, 1, GD_FLOAT64);
    array_push($a, $x[0]);
  }
  array_push($ret["data"], $a);
}

echo json_encode($ret);
?>
