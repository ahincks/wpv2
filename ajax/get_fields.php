<?php
include("all.php");

$df = get_dirfile();

$ret = gd_field_list($df);
if (($e = gd_error($df)))
  return_error("Could not open dirfile: " . gd_error_string($df));

sort($ret);

reply_to_ajax();
?>
