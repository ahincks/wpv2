<?php
include("all.php");

$ret["results"] = array();
if (!(strlen($_POST["search"]) &&
    strpos($_POST["recent"], $_POST["search"]) === false))
  array_push($ret["results"], array("id" => $_POST["recent"],
                                    "text" => $_POST["recent"]));

if ($dh = opendir($dirfile_path)) {
  $df = array();
  while (($de = readdir($dh)) !== false) {
    if (is_dir($dirfile_path . "/" . $de)) {
      if (!is_string($de) || $de == "." || $de == "..")
        continue;
      if (strlen($_POST["search"]) && strpos($de, $_POST["search"]) === false)
        continue;
      array_push($df, $de);
    }
  }
  rsort($df);
  foreach ($df as $de)
    array_push($ret["results"], array("id" => $de, "text" => $de));
}
else
  return_error("Could not open " . $dirfile_path . ".");

reply_to_ajax();
?>
