<?php
//$_POST = $_GET;
include("settings.php");

// We return a JSON object.
header("Content-Type: application/json");

// Getdata settings.
ini_set('getdata.unpack', true);
ini_set('getdata.degrade_complex', true);

// The variable for returning to the AJAX call.
$ret = array();

function get_dirfile() {
  global $ret, $dirfile_path, $curfile;

  // Some temporary hacks.
  //define("GD_FLOAT64", 136);
  //define("GD_RDONLY", 0);

  if (!isset($_POST["dirfile"]))
    return_error("No dirfile information was supplied to the script.");
  if ($_POST["dirfile"] == -1)
    $path = $curfile;
  else
    $path = $dirfile_path . "/" . $_POST["dirfile"];
  $df = gd_open($path, GD_RDONLY);
  if (($e = gd_error($df)))
    return_error("Could not open dirfile: " . gd_error_string($df));

  return $df;
}

function return_error($error) {
  global $ret;

  $ret = array("err" => $error);

  reply_to_ajax();
}

function reply_to_ajax() {
  global $ret;

  echo json_encode($ret);
  exit();
}

?>
