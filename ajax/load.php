<?php
include("all.php");

if (($c = @file_get_contents($save_dir . "/" . $_POST["file"])) === false)
  return_error("Could not find saved file \"" . $_POST["file"] . "\".");

echo str_replace(array(PHP_EOL), array(""), $c);
?>
